\input texinfo   @c -*-texinfo-*-
@c This is part of the book, "GNU Autoconf, Automake and Libtool".
@c Copyright (C) 1999,2000 Gary V. Vaughan, Ben Elliston, Tom Tromey
@c and Ian Lance Taylor.
@c See the file COPYING for copying conditions.
@c %**start of header
@setfilename autobook.info
@settitle Autoconf, Automake, and Libtool
@setchapternewpage odd
@c %**end of header
@finalout
@headings 	    double

m4_ifelse(`

  Pick your favourite from below, or invent your own! =)O|
  Note: no  texinfo escapes (because the text is used inside @menu)
  and no commas (because the text is used in @node).

')m4_dnl
m4_dnl define(COLLECTIVE, `GNU autoconf automake and libtool')m4_dnl
m4_define(COLLECTIVE, `GNU Autotools')m4_dnl
m4_dnl define(COLLECTIVE, `GNU salt')m4_dnl
m4_dnl define(COLLECTIVE, `GNU gestalt')m4_dnl

@include version.texi

@copying
This file documents GNU Autoconf, Automake and Libtool.

Copyright @copyright{} 1999,2000 Gary V. Vaughan, Ben Elliston, Tom Tromey, Ian Lance Taylor

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3;
with no Invariant Sections, with no Front-Cover Texts,
and with no Back-Cover Texts.  A copy of the license is included in
the section entitled ``GNU Free Documentation License''.
@end copying

@dircategory GNU programming tools
@direntry
* Autoconf, Automake, Libtool: (autobook).	Using the GNU autotools.
@end direntry

@titlepage
@sp 10
@title Autoconf, Automake, Libtool
@subtitle Edition @value{EDITION}, @value{UPDATED}
@subtitle $Id$
@include AUTHORS

@page
@vskip 0pt plus 1filll
@insertcopying
@end titlepage

@contents

@ifnottex
@c    name next   previous  up
@node Top, Introduction, (dir), (dir)
@top  The GNU Project Build Tools

@ifinfo
Type @kbd{h} for basic help on navigating this book.
@end ifinfo

@insertcopying
@end ifnottex

@iftex
@unnumbered Foreword
@end iftex

m4_include(chapters/foreword.texi)

@ifnottex
@menu
m4_include(part1.menu)m4_dnl
m4_include(part2.menu)m4_dnl
m4_include(part3.menu)m4_dnl
m4_include(appendices.menu)m4_dnl
* Index::
@end menu
@end ifnottex

@c Part I
@iftex
@page
@unnumbered{Part I}
@end iftex

m4_include(part1.texi)

@c Part II
@iftex
@page
@unnumbered{Part II}
@end iftex

m4_include(part2.texi)

@c Part III
@iftex
@page
@unnumbered{Part III}
@end iftex

m4_include(part3.texi)

@c Appendices
@iftex
@page
@unnumbered{Appendices}
@end iftex

m4_include(appendices.texi)

@page
@node Index
@unnumbered Index

@syncodeindex fn cp
@printindex cp

@summarycontents
@contents
@bye
